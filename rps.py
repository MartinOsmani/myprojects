# Create a RPS game where you play against an AI until someones reaches 3 points.

# The AI makes a random choice between Rock Paper or Scissors
# The human player makes a choice using the input() function
# Run the game in a loop until someones reaches 3 points.
# At every iteration, print the human's choice and the AI's choice, along with the result.
# When you're finished, let me know and we'll push it to Gitlab. Message me in the chat.

import random





AIpoints = 0
userpoints = 0


#User Input
def userInput():
    humanChoice = input("Choose Rock, Paper or Sissors: ")
    if humanChoice in ["Rock", "rock", "R", "r"]:
        humanChoice = "r"
    elif humanChoice in ["Paper", "paper", "P", "p"]:
        humanChoice = "p"
    elif humanChoice in ["Sissors", "sissors", "S", "s"]:
        humanChoice = "s"
    else:
        print("Invalid input, try again.")
        userInput()
    return humanChoice


#AI input
rps = ("r" , "p" , "s")




#Determines who wins the round

while AIpoints < 3 and userpoints < 3:
    print("Your score is:", userpoints ,"the computers score is:", AIpoints)
    AIchoice = random.choice(rps)

    humanChoice = userInput()

    if humanChoice == "r":
        if AIchoice == "r":
            print("You chose Rock, the computer chose Rock. It's a tie")

        elif AIchoice == "p":
            print("You chose Rock, the computer chose Paper. You loose :(")
            AIpoints += 1

        elif AIchoice == "s":
            print("You chose Rock, the computer chose Sissors. You win :)")
            userpoints += 1


    elif humanChoice == "p":
        if AIchoice == "r":
            print("You chose Paper, the computer chose Rock. You win :)")
            userpoints += 1

        elif AIchoice == "p":
            print("You chose Paper, the computer chose Paper. It's a tie")

        elif AIchoice == "s":
            print("You chose Paper, the computer chose Sissors. You loose :(")
            AIpoints += 1


    else:
        if AIchoice == "r":
            print("You chose Sissors, the computer chose Rock. You loose :(")
            AIpoints +=1

        elif AIchoice == "p":
            print("You chose Sissors, the computer chose Paper. You win :)")
            userpoints += 1

        else:
            print("You chose Sissors, the computer chose Sissors. It's a tie")
#Determines who wins the game.
else:
    if AIpoints > userpoints:
        print("Game over, You Lost!")
        print("        :(")
        print("Your score is:", userpoints ,"the computers score is:", AIpoints)
    else:
        print("Game over, You won!")
        print("         :)")
        gameScore = print("Your score is:", userpoints ,"the computers score is:", AIpoints)











